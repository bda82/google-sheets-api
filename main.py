
import os.path
from dotenv import load_dotenv
from pathlib import Path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from data_mapper import SheetDataMapper, DataModel

BASE_DIR = Path(__file__).resolve().parent.parent

load_dotenv(os.path.join(BASE_DIR, ".env"))

SPREADSHEET_ID = str(os.getenv("SPREADSHEET_ID", "1AShhaTPN3fcm_rVx2NW4xF5vK0PQ4zVD5B6f3Hp7yZQ"))
SCOPES_ENV = str(os.getenv("SCOPES", "https://www.googleapis.com/auth/spreadsheets"))
SCOPES_LIST = SCOPES_ENV.split(" ")
SCOPES = [s for s in SCOPES_LIST if s]


class GoogleSheetsRepository:
    spreadsheet_id: str = SPREADSHEET_ID
    scopes: list[str] = SCOPES
    credentials = None

    sheet_name: str = "discounts"
    start_column: str = "A"
    end_column: str = "J"

    headers: list = ["hotel", "branch_id", "room_type_id", "discount", "discount_2",
                     "discount_id", "date_from", "date_to", "city", "created_at"]

    def __init__(self) -> None:
        if os.path.exists("token.json"):
            self.credentials = Credentials.from_authorized_user_file("token.json", self.scopes)
        
        if not self.credentials or not self.credentials.valid:
            if self.credentials and self.credentials.expired and self.credentials.refresh_token:
                self.credentials.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "credentials.json", self.scopes
                )
                self.credentials = flow.run_local_server(port=0)

        with open("token.json", "w") as token:
            token.write(self.credentials.to_json())

    def _get_read_range(self):
        return f"{self.sheet_name}!{self.start_column}:{self.end_column}"

    def _get_update_range(self):
        return f"{self.sheet_name}!{self.start_column}1"

    def _get_length_range(self):
        return f"{self.sheet_name}!{self.start_column}:{self.end_column}"

    def _build_sheet_service(self):
        service = build("sheets", "v4", credentials=self.credentials)

        sheet = service.spreadsheets()

        return sheet
    
    def _get_result(self, sheet, range_):
        sheet_values = sheet.values()
        sheet_values_get = sheet_values.get(spreadsheetId=self.spreadsheet_id, range=range_)
        result = sheet_values_get.execute()

        return result

    @staticmethod
    def _create_body_from_values_list(values_list: list[list]):
        return dict(values=values_list)

    def _update_values(self, sheet, range_, body_to_update):
        sheet_values = sheet.values()

        sheet_values_update = sheet_values.update(
            spreadsheetId=self.spreadsheet_id,
            range=range_,
            valueInputOption="RAW",
            body=body_to_update
        )

        result = sheet_values_update.execute()

        return result

    def get_length(self):
        sheet = self._build_sheet_service()

        range_ = self._get_length_range()

        result = self._get_result(sheet, range_)

        return len(result["values"])

    def update(self, body: list[list]):
        sheet = self._build_sheet_service()

        range_ = self._get_update_range()

        body_to_update = [self.headers] + body
        body_to_update = self._create_body_from_values_list(body_to_update)

        # определим длину диапазона вниз
        length = self.get_length()
        # сравним длину с body_to_update и определим число лишних строк
        extra_rows = length - len(body_to_update["values"])
        # если extra_rows положительна - то допишем пустые строки, чтобы переписать весь диапазон
        if extra_rows > 0:
            body_to_update["values"] += [[""] * len(DataModel().attribute_map)] * extra_rows

        result = self._update_values(sheet, range_, body_to_update)

        return result

    def read(self):
        try:
            sheet = self._build_sheet_service()
            
            range_ = self._get_read_range()
            
            result = self._get_result(sheet, range_)

            sheet_data = result.get("values", [])

            if len(sheet_data) == 0:
                return []

            # remove headers
            sheet_data.pop(0)

            data_mapper = SheetDataMapper(sheet_data)
            mapped_data = data_mapper.get_data()

            return mapped_data

        except HttpError as exc:
            print(exc)


repo = GoogleSheetsRepository()
read_data = repo.read()

for data in read_data:
    data: DataModel
    print(data.to_json())


# моделируем приход queryset моделей с данными
data_to_update_as_queryset = [
    DataModel().from_raw(["1", "2",	"3", "10", "10", "11",
                          "2024-01-01", "2024-02-02", "Taganrog", "2024-01-29 10:00:00"]),
    DataModel().from_raw(["1", "2", "3", "10", "10", "11",
                          "2024-01-01", "2024-02-02", "Taganrog", "2024-01-29 10:00:00"])
]

data_to_update = [
    model.to_list(as_str=True) for model in data_to_update_as_queryset
]

repo.update(data_to_update)

repo.get_length()
