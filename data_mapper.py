import datetime
import uuid


class DataModel:
    attribute_map = (
        {"hotel": int},
        {"branch_id": str},
        {"room_type_id": str},
        {"discount": str},
        {"discount_2": str},
        {"discount_id": str},
        {"date_from": datetime.date},
        {"date_to": datetime.date},
        {"city": str},
        {"created_at": datetime.datetime}
    )

    @staticmethod
    def apply_type(attr_value, type_):
        def is_valid_uuid(value):
            try:
                return uuid.UUID(str(value))
            except ValueError:
                return

        if type_ == str and (uuid_value := is_valid_uuid(attr_value)) is not None:
            return uuid_value
        if type_ in (int, float, str):
            return type_(attr_value)
        if type_ == datetime.date:
            return datetime.datetime.strptime(attr_value, "%Y-%m-%d").date()
        if type_ == datetime.datetime:
            return datetime.datetime.strptime(attr_value, "%Y-%m-%d %H:%M:%S")
        raise ValueError(f"Unsupported type: {type_}")

    def append_attr(self, name, attr_value, type_):
        try:
            value_to_set = self.apply_type(attr_value, type_)
            setattr(self, name, value_to_set)
        except Exception as exc:
            print(f"Cant set attribute [{name}] with type [{type_}] and value [{attr_value}]. "
                  f"Get an exception: {exc}")

    def from_raw(self, data_raw: list):
        if len(data_raw) < 1:
            raise ValueError("Data is empty")

        for i in range(len(data_raw)):
            name_ = tuple(self.attribute_map[i].keys())[0]
            type_ = self.attribute_map[i][name_]
            self.append_attr(name=name_, attr_value=data_raw[i], type_=type_)

        return self

    def to_json(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith("_")}

    def to_list(self, as_str: bool = False):
        if as_str:
            return [str(v) for k, v in self.__dict__.items() if not k.startswith("_")]
        return [v for k, v in self.__dict__.items() if not k.startswith("_")]


class SheetDataMapper:
    sheet_data: list

    def __init__(self, sheet_data: list[list]):
        self.sheet_data = []
        for data_raw in sheet_data:
            self.from_row(data_raw)

    def from_row(self, data_raw):
        self.sheet_data.append(DataModel().from_raw(data_raw))

    def get_data(self) -> list:
        return self.sheet_data
